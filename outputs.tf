
#Outputs
output "app1_name" {
  value = module.app1_vpc.name
}

output "app1_region" {
  value = var.region
}

output "app1_vpc_id" {
  value = module.app1_vpc.vpc_id
}

output "app1_gw_subnet_cidr" {
  value = module.app1_vpc.public_subnets_cidr_blocks[0]
}

output "app1_hagw_subnet_cidr" {
  value = module.app1_vpc.public_subnets_cidr_blocks[1]
}

output "app2_name" {
  value = module.app2_vpc.name
}

output "app2_region" {
  value = var.region
}

output "app2_vpc_id" {
  value = module.app2_vpc.vpc_id
}

output "app2_gw_subnet_cidr" {
  value = module.app2_vpc.public_subnets_cidr_blocks[0]
}

output "app2_hagw_subnet_cidr" {
  value = module.app2_vpc.public_subnets_cidr_blocks[1]
}

output "account_name" {
  value = "DevOps_Team_1"
}

data "aws_caller_identity" "current" {}
output "account_id" {
  value = data.aws_caller_identity.current.account_id
}