#App1
module "app1_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "App1"
  cidr = "10.10.1.0/24"

  azs             = ["${var.region}a", "${var.region}b", ]
  private_subnets = ["10.10.1.0/26", "10.10.1.64/26", ]
  public_subnets  = ["10.10.1.224/28", "10.10.1.240/28", ]
}

#App2
module "app2_vpc" {
  providers = { aws = aws.region2 }
  source = "terraform-aws-modules/vpc/aws"

  name = "App2"
  cidr = "10.10.2.0/24"

  azs             = ["${var.region2}a", "${var.region2}b", ]
  private_subnets = ["10.10.2.0/26", "10.10.2.64/26", ]
  public_subnets  = ["10.10.2.224/28", "10.10.2.240/28", ]
}

# cross account 
module "iam_roles" {
  source                         = "github.com/AviatrixSystems/terraform-modules.git//aviatrix-controller-iam-roles?ref=terraform_0.14"
  external-controller-account-id = "983531232307"
  ec2_role_name                  = "demo-ec2-role" #Only used for demo, to prevent role name collisions
  app_role_name                  = "demo-app-role" #Only used for demo, to prevent role name collisions
}